/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modul4;
import java.util.Calendar;
import java.util.GregorianCalendar;
import static modul4.Compformat.c;
/**
 *
 * @author er_herlin
 */
public class Noncompformat {
    static Calendar c = new GregorianCalendar(1995, GregorianCalendar.MAY, 23);
    public static void main(String[] args) {
        //args[0] should contain the credit card expiration date
        //Perform comparison with c,
        //if it doesn't match, print the following line
        System.out.format(args[0] + " did not match! HINT : It was issued on %1$terd of some month", c);
    }
}