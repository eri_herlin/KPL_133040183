/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modul4;
import java.io.InputStream;
/**
 *
 * @author eri_herlin
 */
public class Noncompdirlist_posix {
    public static void main(String[] args) throws Exception {
        String dir = System.getProperty("dir");
        Runtime rt = Runtime.getRuntime();
        Process proc = rt.exec(new String[]{"sh", "-c", "ls " + dir});
        int result = proc.waitFor();
        if (result != 0) {
            System.out.println("process error: " + result);
        }
        InputStream in = (result == 0) ? proc.getInputStream() :
                proc.getErrorStream();
        int c;
        while ((c = in.read()) != -1) {
            System.out.print((char) c);
        }
    }
}